import React from 'react'
import { Text } from 'react-native'

export const MyText = ({ left, right, center, size, children }) => {
  return (
    <Text
      style={{
        fontSize: size,
        textAlign: left ? 'left' : right ? 'right' : 'center'
      }}
    >
      {children}
    </Text>
  )
}

// export const MyText = (props) => {
//     return (
//       <Text
//         style={{
//           fontSize: props.size,
//           textAlign: props.left ? 'left' : props.right ? 'right' : 'center'
//         }}
//       >
//         {props.children}
//       </Text>
//     )
//}

// export { MyText }

// export default MyText
