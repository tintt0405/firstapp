import React from 'react'
import { View } from 'react-native'

// custom component
import { MyText } from '../components/MyText'

class Screen extends React.Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          //   alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'salmon'
        }}
      >
        <MyText right size={16}>
          Hello World
        </MyText>
      </View>
    )
  }
}

export default Screen
