import React from 'react'
import { Text, View, ScrollView } from 'react-native'

const arr = new Array(10).fill('This is item number ')
const obj = { item1: 1, item2: 2, item3: 3, item4: 4, item5: 5 }

export default class Screen extends React.Component {
  mapArray = (value, index) => {
    return (
      <Text style={{ margin: 16 }}>
        {value} : {index}
      </Text>
    )
  }

  mapObjectKeys = (value, index) => {
    return (
      <Text style={{ margin: 16 }}>
        {value}
      </Text>
    )
  }

  mapObjectValues = (value, index, myCustomMessage) => {
    return (
      <Text style={{ margin: 16 }}>
        {myCustomMessage} : {value} : {index}
      </Text>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, paddingLeft: 16, backgroundColor: 'green' }}>
        <ScrollView>
          <Text>
            {'Map array'.toUpperCase()}
          </Text>

          {arr.map(this.mapArray)}

          <Text>
            {'Map object keys'.toUpperCase()}
          </Text>

          {Object.keys(obj).map(this.mapObjectKeys)}

          <Text>
            {'Map object values'.toUpperCase()}
          </Text>

          {Object.values(obj).map((value, index) =>
            this.mapObjectValues(value, index, 'My message')
          )}
        </ScrollView>
      </View>
    )
  }
}
