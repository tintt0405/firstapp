import React from 'react'
import { View, Text, TextInput, Button } from 'react-native'

const USERNAME = '1'
const PASSWORD = '1'

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }

    // bind class context to function
    this.onUsernameChange = this.onUsernameChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
  }

  onUsernameChange(username) {
    this.setState({ username: username })
  }

  onPasswordChange(password) {
    this.setState({ password }) // ES6
  }

  // ES6
  onPress = () => {
    const username = this.state.username
    const { password } = this.state // ES6

    if (username === USERNAME && password === PASSWORD) {
      alert('Login success')
    } else {
      alert('Login fail')
    }
  }

  render() {
    const styles = {
      margin: 16,
      paddingLeft: 16,
      borderRadius: 16,
      borderWidth: 1,
      borderColor: '#EAEAEA'
    }

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'white'
        }}
      >
        <TextInput
          placeholder="Username"
          style={styles}
          onChangeText={this.onUsernameChange}
        />

        <TextInput
          placeholder="Password"
          style={styles}
          onChangeText={this.onPasswordChange}
        />

        <Button title="Login" color="dodgerblue" onPress={this.onPress} />
      </View>
    )
  }
}
