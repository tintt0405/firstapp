/** @format */

import { AppRegistry } from 'react-native'
import Welcome from './src/containers'
import { name as appName } from './app.json'

AppRegistry.registerComponent(appName, () => Welcome)
